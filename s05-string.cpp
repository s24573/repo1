#include <iostream>
#include <cstring>

auto main(int argc, char*argv[]) -> int 
{
    auto const cenzura = std::string{argv[1]};
    auto const litery = std::string{argv[2]};
    
    char arr1[cenzura.length()];
    strcpy(arr1,cenzura.c_str());
    
    char arr2[litery.length()];
    strcpy(arr2,litery.c_str());
    
    int x = 0;
    
    if(cenzura.empty())
        {
            return -1;
        }
    if(litery.empty())
        {
            std::cout << cenzura;
            return 0;

        }
    for(int i = 0; i < cenzura.length(); i++)
        {
        if(arr1[i] == '*')
            {
            arr1[i] = arr2[x];
            x++; 
            }
        std::cout << arr1[i];
        }
    return 0;
}
