#include <iostream>

auto main(int argc, char*argv[]) -> int
{
    int number = std::atoi(argv[1]);

while (number > 0)
    {
    std::cout << number << std::endl;
    if (number % 3 == 0 && number % 5 == 0)
        std::cout << "FizzBuzz" << std::endl;
    else if(number % 3 == 0)
        std::cout << "Fizz" << std::endl;
    else if (number % 5 == 0)
        std::cout << "Buzz" << std::endl;

    number--;
    }
return 0;
}