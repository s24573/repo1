#include <iostream>
#include <string>

int main()
{
    int number;
    std::cout << "Podaj liczbe od ktorej ma sie zaczac odliczanie: ";
    std::cin >> number;
    if (number > 0)
    {
    int countdown = - 1;
    while (number > countdown)
    {
        number--;
        std::cout << number + 1 << std::endl;
    }
    }
    return 0;
}