#include <iostream>

auto main(int argc, char*argv[]) -> int
{
    int vowels;

    vowels = 0;

    auto const sentence = std::string{argv[1]};
    
    for(int i = 0; sentence[i]!='\0'; ++i)
    {
        if(sentence[i]=='a' || sentence[i]=='e' || sentence[i]=='i' ||
           sentence[i]=='o' || sentence[i]=='u' || sentence[i]=='A' ||
           sentence[i]=='E' || sentence[i]=='I' || sentence[i]=='O'||
           sentence[i]=='U')
        {
            ++vowels;
        }
    }

    std::cout << vowels << std::endl;

    return 0;
}
