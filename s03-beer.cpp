#include <iostream>

auto main(int argc, char*argv[]) -> int
{
    int bottles = std::atoi(argv[1]);
    std::cout << bottles << " bottles of beer on the wall," << std::endl;
    while (bottles > 1)
        {
        std::cout << bottles << " bottles of beer." << std::endl;
        std::cout << "  Take one down, pass it around," << std::endl;
        std::cout << --bottles << " bottles of beer on the wall." << std::endl;
        }
    if (bottles == 1);
        {
        std::cout << bottles << " bottle of beer." << std::endl;
        std::cout << "  Take one down, pass it around," << std::endl;
        --bottles;
        }
    if (bottles == 0);
        {
        std::cout << "  No more bottles of beer on the wall, no more bottles of beer." << std::endl;
        std::cout << "  Go to the store and buy some more, 99 bottles of beer on the wall..." << std::endl;
        }
    return 0;
}
