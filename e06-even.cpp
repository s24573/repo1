#include <iostream>

auto main(int argc, char*argv[]) -> int 
{
	int suma = 0;
	for(int i = 1; i < argc; i++)
	{
		if (std::atoi(argv[i]) % 2 == 0)
		{
			suma += std::atoi(argv[i]);
		}
	}
 	std::cout << suma << std::endl;
  	return 0;
}
