#include <iostream>
#include <ctime>

int main()
{
	int number, guess;
	std::srand(std::time(0));
    number = rand() % 100 + 1;
	std::cout << "Gra w zgadywanie" << std::endl;

	do
	{
		std::cout << "Zgadnij liczbe od 1 do 100: ";
		std::cin >> guess;

		if (guess > number)
			std::cout << "Za duza liczba!" << std::endl;
		else if (guess < number)
			std::cout << "Za mala liczba!" << std::endl;
		else
			std::cout << "Brawo, udalo ci sie!" << std::endl;
	} 
    while (guess != number);
    {
	return 0;
    }
}
