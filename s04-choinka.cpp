#include <iostream>

auto main(int argc, char*argv[]) -> int
{
    int gap;
    int h = std::atoi(argv[1]);
    
    for(int i = 1, stars = 0; i <= h; ++i, stars = 0)
    {
        for(gap = 1; gap <= h-i; ++gap)
        {
            std::cout <<" ";
        }

        while(stars != 2*i-1)
        {
            std::cout << "*";
            ++stars;
        }
        std::cout << std::endl;
    }
    return 0;
}
